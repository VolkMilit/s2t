use std::path::PathBuf;
use clap::{Arg, ArgAction, Command};
use anyhow::anyhow;
use stapi::Telegram;

fn main() -> anyhow::Result<()> {
    let commands = Command::new("s2t")
        .about("Send anything to Telegram.")
        .version(env!("CARGO_PKG_VERSION"))
        .arg_required_else_help(true)
        .arg(
            Arg::new("message")
                .short('m')
                .long("message")
                .conflicts_with("files")
                .action(ArgAction::Set)
                .num_args(1)
                .help("Send an message to Telegram.")
        )
        .arg(
            Arg::new("files")
                .short('f')
                .long("files")
                .conflicts_with("message")
                .action(ArgAction::Set)
                .num_args(1..)
                .help("Send an file(s) to Telegram.")
        )
        .arg(
            Arg::new("token")
                .short('t')
                .long("token")
                .action(ArgAction::Set)
                .num_args(1)
                .help("Set Telegram token for private chat.")
        )
        .arg(
            Arg::new("chatid")
                .short('i')
                .long("chatid")
                .action(ArgAction::Set)
                .num_args(1)
                .help("Set Telegram chat id.")
        )
        .arg(
            Arg::new("url")
                .short('u')
                .action(ArgAction::Set)
                .num_args(1)
                .help("Set custom Telegram url (default is https://api.telegram.org).")
        )
        .get_matches();

    let chat_id = match commands.get_one::<String>("chatid") {
        Some(chat_id) => chat_id,
        None => return Err(anyhow!("Chat id must be presented!"))
    };

    let token = match commands.get_one::<String>("token") {
        Some(token) => token,
        None => return Err(anyhow!("Token must be presented!"))
    };

    let ci = format!("-100{chat_id}");
    let mut telegram = Telegram::create_handle(token, &ci);

    if let Some(url) = commands.get_one::<String>("url") {
        telegram.set_url(url);
    }

    if let Some(message) = commands.get_one::<String>("message") {
        telegram.send_message(message)?;
    }
    else if let Some(files) = commands.get_many::<String>("files") {
        for file in files {
            let file = PathBuf::from(file);

            if file.exists() {
                telegram.send_document(file)?;
            }
            else {
                println!("{:#?} doesn't exist!", file);
            }
        }
    }

    Ok(())
}
